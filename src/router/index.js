import Vue from 'vue'
import VueRouter from 'vue-router'
import LoginPage from '../views/LoginPage.vue'
import RegisterPage from '../views/RegisterPage.vue'
import MainPage from '../views/MainPage.vue'
import EventPage from '../views/EventPage.vue'
import EventIdPage from '../views/EventIdPage.vue'
import Profile from '../views/Profile.vue'
import CreateEvent from '../views/CreateEvent'
import ConfirmAccountHandler from '../views/ConfirmAccountHandler'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'MainPage',
    component: MainPage
  },
  {
    path: '/about',
    name: 'About',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  },
  {
    path: '/login',
    name: 'LoginPage',
    component: LoginPage
  },
  {
    path: '/registration',
    name: 'RegisterPage',
    component: RegisterPage
  },
  {
    path: '/events',
    name: 'EventPage',
    component: EventPage
  },
  {
    path: '/events/:id',
    name: 'EventIdPage',
    component: EventIdPage
  },
  {
    path: '/profile',
    name: 'Profile',
    component: Profile
  },
  {
    path: '/create-event',
    name: 'CreateEvent',
    component: CreateEvent
  },
  {
    path: '/confirm-account',
    name: 'ConfirmAccountHandler',
    component: ConfirmAccountHandler
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
