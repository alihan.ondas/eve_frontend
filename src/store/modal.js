import { MODAL } from './types'
export default {
  state: () => ({
    isModalOpen: false,
    modalContent: '',
    icon: ''
  }),
  getters: {
    [MODAL.GETTER.GET_IS_MODAL_OPEN]: (state) => state.isModalOpen,
    [MODAL.GETTER.GET_MODAL_CONTENT]: (state) => state.modalContent,
    [MODAL.GETTER.GET_MODAL_ICON]: (state) => state.icon
  },
  mutations: {
    [MODAL.MUTATION.SET_IS_MODAL_OPEN]: (state, value) => (state.isModalOpen = value),
    [MODAL.MUTATION.SET_MODAL_CONTENT]: (state, value) => (state.modalContent = value),
    [MODAL.MUTATION.SET_MODAL_ICON]: (state, value) => (state.icon = value)
  }
}
