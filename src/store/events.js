import { EVENTS, PROFILE } from './types'
import axios from 'axios'
import Vue from 'vue'
import { cloneDeep } from 'lodash'
const api = process.env.VUE_APP_API

export default {
  state: () => ({
    /* for codes see helpers/index mapDataCode{} */
    currentDateTab: 1,
    popularEvents: [],
    recommendedEvents: [],
    currentEventData: null,
    myEvents: [],
    reviews: null
  }),
  getters: {
    [EVENTS.GETTER.GET_CURRENT_DATE_TAB]: state => state.currentDateTab,
    [EVENTS.GETTER.GET_POPULAR_EVENTS]: state => cloneDeep(state.popularEvents),
    [EVENTS.GETTER.GET_RECOMMENDED_EVENTS]: state => cloneDeep(state.recommendedEvents),
    [EVENTS.GETTER.GET_CURRENT_EVENT]: state => cloneDeep(state.currentEventData),
    [EVENTS.GETTER.GET_MY_EVENTS]: state => cloneDeep(state.myEvents),
    [EVENTS.GETTER.GET_EVENT_REVIEWS]: state => eventId => {
      if (!state.reviews) return null
      if (!state.reviews[eventId]) return null
      return cloneDeep(state.reviews[eventId])
    }
  },
  mutations: {
    [EVENTS.MUTATION.SET_CURRENT_DATE_TAB]: (state, value) => (state.currentDateTab = value),
    [EVENTS.MUTATION.SET_POPULAR_EVENTS]: (state, value) => (state.popularEvents = cloneDeep(value)),
    [EVENTS.MUTATION.SET_RECOMMENDED_EVENTS]: (state, value) => (state.recommendedEvents = cloneDeep(value)),
    [EVENTS.MUTATION.SET_CURRENT_EVENT]: (state, value) => (state.currentEventData = cloneDeep(value)),
    [EVENTS.MUTATION.SET_MY_EVENTS]: (state, value) => (state.myEvents = cloneDeep(value)),
    [EVENTS.MUTATION.SET_EVENT_REVIEWS]: (state, { eventId, reviews }) => {
      if (!state.reviews) state.reviews = {}
      if (!state.reviews[eventId]) Vue.set(state.reviews, eventId, [])
      state.reviews[eventId] = reviews
    }
  },
  actions: {
    // async [EVENTS.ACTION.POST_REVIEW] ({}, ) {

    // },
    async [EVENTS.ACTION.LOAD_REVIEWS_EVENTS] ({ commit }, eventId) {
      try {
        const res = await axios.get(`${api}/review?eventId=${eventId}`)
        commit(EVENTS.MUTATION.SET_EVENT_REVIEWS, { eventId, reviews: res.data })
      } catch (err) {
        console.error(err)
      }
    },
    async [EVENTS.ACTION.LOAD_POPULAR_EVENTS] ({ commit }) {
      try {
        const res = await axios.get(`${api}/event/popular?sort=date&order=asc`)
        commit(EVENTS.MUTATION.SET_POPULAR_EVENTS, res.data)
      } catch (err) {
        console.error(err)
      }
    },
    async [EVENTS.ACTION.LOAD_EVENT_BY_ID] ({ commit }, id) {
      try {
        const res = await axios.get(`${api}/event/${id}`)
        commit(EVENTS.MUTATION.SET_CURRENT_EVENT, res.data)
      } catch (err) {
        console.error(err)
      }
    },
    async [EVENTS.ACTION.ATTEND_EVENT] ({ getters, dispatch }, { eventId }) {
      try {
        const userId = getters[PROFILE.GETTER.GET_USER_DATA].id
        await axios.post(`${api}/event/attend?eventId=${eventId}&userId=${userId}`)
        await dispatch(EVENTS.ACTION.LOAD_EVENT_BY_ID, eventId)
        await dispatch(EVENTS.ACTION.LOAD_MY_EVENTS)
      } catch (err) {
        console.error(err)
      }
    },
    async [EVENTS.ACTION.UNATTEND_EVENT] ({ getters, dispatch }, { eventId }) {
      try {
        const userId = getters[PROFILE.GETTER.GET_USER_DATA].id
        await axios.delete(`${api}/event/unattend?eventId=${eventId}&userId=${userId}`)
        await dispatch(EVENTS.ACTION.LOAD_EVENT_BY_ID, eventId)
        await dispatch(EVENTS.ACTION.LOAD_MY_EVENTS)
      } catch (err) {
        console.error(err)
      }
    },
    async [EVENTS.ACTION.LOAD_MY_EVENTS] ({ getters, commit }) {
      try {
        const userId = getters[PROFILE.GETTER.GET_USER_DATA].id
        const res = await axios.get(`${api}/my_events?id=${userId}`)
        commit(EVENTS.MUTATION.SET_MY_EVENTS, res.data)
      } catch (err) {
        console.error(err)
      }
    },
    async [EVENTS.ACTION.CREATE_EVENT] ({ getters }, value) {
      try {
        const userData = getters[PROFILE.GETTER.GET_USER_DATA]
        await axios.post(`${api}/event`, { ...value, author: userData })
        // await axios.post(`${api}/event`, { title: 'kek' })
      } catch (err) {
        console.error(err)
        throw err
      }
    },
    async [EVENTS.ACTION.LOAD_RECOMMENDED_EVENTS] ({ getters }) {
      try {
        const userId = getters[PROFILE.GETTER.GET_USER_DATA].id
        await axios.get(`${api}/event/recommended?userId=${userId}`)
      } catch (err) {
        console.error(err)
      }
    },
    async [EVENTS.ACTION.LOAD_ATTENDING_FRIENDS] ({ getters }, { eventId }) {
      try {
        const userId = getters[PROFILE.GETTER.GET_USER_DATA].id
        return (await axios.get(`${api}/event/get_friends_attending_event?eventId=${eventId}&userId=${userId}`))
      } catch (err) {
        console.error(err)
      }
    }
  }
}
