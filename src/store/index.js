import Vue from 'vue'
import Vuex from 'vuex'
import PROFILE from './profile'
import MODAL from './modal'
import EVENTS from './events'
import REVIEW from './review'
import FRIENDS from './friends'
import createPersistedState from "vuex-persistedstate"

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    PROFILE,
    MODAL,
    EVENTS,
    REVIEW,
    FRIENDS
  },
  plugins: [createPersistedState()]
})
