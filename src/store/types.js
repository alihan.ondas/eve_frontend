export const PROFILE = {
  GETTER: {
    GET_TOKEN: `PROFILE/GETTER/GET_TOKEN`,
    GET_IS_AUTHORIZED: `PROFILE/GETTER/GET_IS_AUTHORIZED`,
    GET_USER_DATA: `PROFILE/GETTER/GET_IS_AUTHORIZED`,
    GET_LOGIN_LOADING: `PROFILE/GETTER/GET_LOGIN_LOADING`,
    GET_REGISTER_LOADING: `PROFILE/MUTATION/GET_REGISTER_LOADING`
  },
  MUTATION: {
    SET_TOKEN: `PROFILE/MUTATION/SET_TOKEN`,
    SET_IS_AUTHORIZED: `PROFILE/MUTATION/SET_IS_AUTHORIZED`,
    SET_USER_DATA: `PROFILE/MUTATION/SET_USER_DATA`,
    LOGOUT: `PROFILE/MUTATION/LOGOUT`,
    SET_LOGIN_LOADING: `PROFILE/MUTATION/SET_LOGIN_LOADING`,
    SET_REGISTER_LOADING: `PROFILE/MUTATION/SET_REGISTER_LOADING`
  },
  ACTION: {
    REGISTER: `PROFILE/ACTION/REGISTER`,
    LOGIN: `PROFILE/ACTION/LOGIN`,
    UPDATE_PROFILE: `PROFILE/ACTION/UPDATE_PROFILE`,
    LOAD_PROFILE: `PROFILE/ACTION/LOAD_PROFILE`
  }
}

export const MODAL = {
  GETTER: {
    GET_IS_MODAL_OPEN: `MODAL/GETTER/GET_IS_MODAL_OPEN`,
    GET_MODAL_CONTENT: `MODAL/GETTER/GET_MODAL_CONTENT`,
    GET_MODAL_ICON: `MODAL/GETTER/GET_MODAL_ICON`
  },
  MUTATION: {
    SET_IS_MODAL_OPEN: `MODAL/MUTATION/SET_IS_MODAL_OPEN`,
    SET_MODAL_CONTENT: `MODAL/MUTATION/SET_MODAL_CONTENT`,
    SET_MODAL_ICON: `MODAL/MUTATION/SET_MODAL_ICON`
  }
}

export const EVENTS = {
  GETTER: {
    GET_CURRENT_DATE_TAB: `EVENTS/GETTER/GET_CURRENT_DATE_TAB`,
    GET_POPULAR_EVENTS: `EVENTS/GETTER/GET_POPULAR_EVENTS`,
    GET_RECOMMENDED_EVENTS: `EVENTS/GETTER/GET_RECOMMENDED_EVENTS`,
    GET_CURRENT_EVENT: `EVENTS/GETTER/GET_CURRENT_EVENT`,
    GET_MY_EVENTS: `EVENTS/GETTER/GET_MY_EVENTS`,
    GET_EVENT_REVIEWS: `EVENTS/GETTER/GET_EVENT_REVIEWS`
  },
  MUTATION: {
    SET_CURRENT_DATE_TAB: `EVENTS/MUTATION/SET_CURRENT_DATE_TAB`,
    SET_POPULAR_EVENTS: `EVENTS/MUTATION/SET_POPULAR_EVENTS`,
    SET_RECOMMENDED_EVENTS: `EVENTS/MUTATION/SET_RECOMMENDED_EVENTS`,
    SET_CURRENT_EVENT: `EVENTS/MUTATION/SET_CURRENT_EVENT`,
    SET_MY_EVENTS: `EVENTS/MUTATION/SET_MY_EVENTS`,
    SET_EVENT_REVIEWS: `EVENTS/MUTATION/SET_EVENT_REVIEWS`
  },
  ACTION: {
    LOAD_POPULAR_EVENTS: `EVENTS/ACTION/LOAD_POPULAR_EVENTS`,
    LOAD_RECOMMENDED_EVENTS: `EVENTS/ACTION/LOAD_RECOMMENDED_EVENTS`,
    LOAD_EVENT_BY_ID: `EVENTS/ACTION/LOAD_EVENT_BY_ID`,
    ATTEND_EVENT: `EVENTS/ACTION/ATTEND_EVENT`,
    UNATTEND_EVENT: `EVENTS/ACTION/UNATTEND_EVENT`,
    LOAD_MY_EVENTS: `EVENTS/ACTION/LOAD_MY_EVENTS`,
    CREATE_EVENT: `EVENTS/ACTION/CREATE_EVENT`,
    LOAD_REVIEWS_EVENTS: `EVENTS/ACTION/LOAD_REVIEWS_EVENTS`,
    LOAD_ATTENDING_FRIENDS: `EVENTS/ACTION/LOAD_ATTENDING_FRIENDS`
  }
}

export const REVIEW = {
  GETTER: {
    GET_CURRENT_EVENT_ID: `REVIEW/GETTER/GET_CURRENT_EVENT_ID`
  },
  MUTATION: {
    SET_CURRENT_EVENT_ID: `REVIEW/MUTATION/SET_CURRENT_EVENT_ID`
  },
  ACTION: {
    POST_REVIEW: `REVIEW/ACTION/POST_REVIEW`
  }
}

export const FRIENDS = {
  GETTER: {
    GET_FRIENDS: `FRIENDS/GETTER/GET_FRIENDS`,
    GET_ADD_FRIEND_MODAL: `FRIENDS/GETTER/GET_ADD_FRIEND_MODAL`
  },
  MUTATION: {
    SET_FRIENDS: `FRIENDS/MUTATION/SET_FRIENDS`,
    SET_ADD_FRIEND_MODAL: `FRIENDS/MUTATION/SET_ADD_FRIEND_MODAL`
  },
  ACTION: {
    LOAD_FRIENDS: `FRIENDS/ACTION/LOAD_FRIENDS`,
    FIND_FRIENDS: `FRIENDS/ACTION/FIND_FRIENDS`,
    ADD_FRIEND: `FRIENDS/ACTION/ADD_FRIEND`
  }
}
