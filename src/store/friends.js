import { FRIENDS, PROFILE } from './types'
import axios from 'axios'
const api = process.env.VUE_APP_API

export default {
  state: () => ({
    friendList: null,
    addFriendModalOpen: false
  }),
  getters: {
    [FRIENDS.GETTER.GET_FRIENDS]: state => state.friendList,
    [FRIENDS.GETTER.GET_ADD_FRIEND_MODAL]: state => state.addFriendModalOpen
  },
  mutations: {
    [FRIENDS.MUTATION.SET_FRIENDS]: (state, frndLst) => (state.friendList = frndLst),
    [FRIENDS.MUTATION.SET_ADD_FRIEND_MODAL]: (state, value) => (state.addFriendModalOpen = value)
  },
  actions: {
    async [FRIENDS.ACTION.LOAD_FRIENDS] ({ commit, getters }) {
      try {
        const userId = getters[PROFILE.GETTER.GET_USER_DATA].id
        const res = await axios.get(`${api}/user/friends?userId=${userId}`)
        commit(FRIENDS.MUTATION.SET_FRIENDS, res.data)
        console.log('res friends', res)
      } catch (err) {
        console.error(err)
      }
    },
    async [FRIENDS.ACTION.FIND_FRIENDS] (_, { name }) {
      try {
        const res = await axios.get(`${api}/admin/users?name=${name}`)
        console.log(res)
        return res
      } catch (err) {
        console.error(err)
      }
    },
    async [FRIENDS.ACTION.ADD_FRIEND] (_, { userId, friendId }) {
      try {
        await axios.post(`${api}/user/add_friend?userId=${userId}&friendId=${friendId}`)
      } catch (err) {
        console.error(err)
      }
    }
  }
}
