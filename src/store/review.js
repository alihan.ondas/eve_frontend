import axios from "axios"
import { REVIEW, PROFILE } from "./types"

const api = process.env.VUE_APP_API
export default {
  state: () => ({
    currentEventId: null
  }),
  getters: {
    [REVIEW.GETTER.GET_CURRENT_EVENT_ID]: state => state.currentEventId
  },
  mutations: {
    [REVIEW.MUTATION.SET_CURRENT_EVENT_ID]: (state, eventId) => (state.currentEventId = eventId)
  },
  actions: {
    async [REVIEW.ACTION.POST_REVIEW] ({ getters, state }, { rating, text }) {
      try {
        const userId = getters[PROFILE.GETTER.GET_USER_DATA].id
        await axios.post(`${api}/review`, {
          rating,
          text,
          userId,
          eventId: state.currentEventId
        })
      } catch (err) {
        console.error(err)
        throw err
      }
    }
  }
}
