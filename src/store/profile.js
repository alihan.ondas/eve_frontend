import { EVENTS, PROFILE } from './types'
import axios from 'axios'

const api = process.env.VUE_APP_API

export default {
  state: () => ({
    userData: null,
    token: null,
    isAuthorized: false,
    loginLoading: false,
    registerLoading: false
  }),
  getters: {
    [PROFILE.GETTER.GET_IS_AUTHORIZED]: state => state.isAuthorized,
    [PROFILE.GETTER.GET_TOKEN]: state => state.token,
    [PROFILE.GETTER.GET_USER_DATA]: state => state.userData,
    [PROFILE.GETTER.GET_LOGIN_LOADING]: state => state.loginLoading,
    [PROFILE.GETTER.GET_REGISTER_LOADING]: state => state.registerLoading
  },
  mutations: {
    [PROFILE.MUTATION.SET_TOKEN] (state, value) {
      state.token = value
    },
    [PROFILE.MUTATION.SET_IS_AUTHORIZED] (state, value) {
      state.isAuthorized = value
    },
    [PROFILE.MUTATION.SET_USER_DATA] (state, value) {
      state.userData = { ...state.userData, ...value }
    },
    [PROFILE.MUTATION.LOGOUT] (state) {
      state.isAuthorized = false
      state.token = null
      state.userData = null
    },
    [PROFILE.MUTATION.SET_LOGIN_LOADING] (state, value) {
      state.loginLoading = value
    },
    [PROFILE.MUTATION.SET_REGISTER_LOADING] (state, value) {
      state.registerLoading = value
    }
  },
  actions: {
    async [PROFILE.ACTION.REGISTER] ({ state, commit }, value) {
      try {
        commit(PROFILE.MUTATION.SET_REGISTER_LOADING, true)
        await axios.post(`${api}/registration`, value)

        commit(PROFILE.MUTATION.SET_REGISTER_LOADING, false)
        return { error: false, value }
      } catch (err) {
        console.error(err)
        commit(PROFILE.MUTATION.SET_REGISTER_LOADING, false)
        return { error: true, err: (err && err.response && err.response.data && err.response.data.message) || err, value }
      }
    },
    async [PROFILE.ACTION.LOGIN] ({ commit, dispatch }, value) {
      try {
        commit(PROFILE.MUTATION.SET_LOGIN_LOADING, true)
        const res = await axios.post(`${api}/login`, value)
        console.log('res login', res)
        commit(PROFILE.MUTATION.SET_TOKEN, res.data[0])
        commit(PROFILE.MUTATION.SET_USER_DATA, res.data[1])
        commit(PROFILE.MUTATION.SET_IS_AUTHORIZED, true)
        commit(PROFILE.MUTATION.SET_LOGIN_LOADING, false)
        dispatch(EVENTS.ACTION.LOAD_RECOMMENDED_EVENTS)
        dispatch(EVENTS.ACTION.LOAD_MY_EVENTS)
        return { error: false }
      } catch (err) {
        console.error(err)
        commit(PROFILE.MUTATION.SET_LOGIN_LOADING, false)
        return { error: true, err: (err && err.response && err.response.data && err.response.data.message) || err }
      }
    },
    async [PROFILE.ACTION.UPDATE_PROFILE] ({ state, dispatch }, value) {
      try {
        const res = await axios.put(`${api}/profile`, {
          ...state.userData,
          ...value
        })
        await dispatch(PROFILE.ACTION.LOAD_PROFILE, state.userData.id)
        console.log('update prof res', res)
      } catch (err) {
        console.error(err)
        await dispatch(PROFILE.ACTION.LOAD_PROFILE, state.userData.id)
        return { error: true, err }
      }
    },
    async [PROFILE.ACTION.LOAD_PROFILE] ({ commit }, id) {
      try {
        const res = await axios.get(`${api}/profile?id=${id}`)
        commit(PROFILE.MUTATION.SET_USER_DATA, res.data)
        console.log('load prof res', res.data)
      } catch (err) {
        console.error('err', err)
        return { error: true, err }
      }
    }
  }
}
