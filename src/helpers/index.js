export const mapDateTitle = {
  1: 'All',
  2: 'Today',
  3: 'This Week',
  4: 'This Month'
}

export const mapDateAmount = {
  1: 999,
  2: 1,
  3: 7,
  4: 30
}

export const checkDate = (date, sortCode) => {
  if (sortCode === 1) return true
  if (!date) return false
  const currentDate = new Date()
  const middleDate = new Date()
  middleDate.setFullYear(date[0])
  middleDate.setMonth(date[1] - 1)
  middleDate.setDate(date[2])
  middleDate.setHours(date[3])
  middleDate.setMinutes(date[4])
  middleDate.setSeconds(date[5])
  const lastDate = new Date(currentDate)
  lastDate.setDate(lastDate.getDate() + mapDateAmount[sortCode])

  if (currentDate.getTime() <= middleDate.getTime() && middleDate.getTime() <= lastDate.getTime()) return true
  else return false
}

export const fillDate = () => {
  const copy = new Date()
  copy.setDate(copy.getDate() + 1)
  return copy.toJSON().slice(0, 16)
}
